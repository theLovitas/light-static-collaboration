# Light Static Collaboration

This project contains the bot for the 'Light Static Collaboration' Discord server.

The bot uses a [redis cache](https://redis.io/) (default configuration).
To start the bot locally you need a running redis cache and provide a token and correct channel ids in the `config.json`.