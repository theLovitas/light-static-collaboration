package eu.lovitas.lsc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import discord4j.common.util.Snowflake;
import discord4j.core.DiscordClient;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.guild.MemberLeaveEvent;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.event.domain.message.MessageDeleteEvent;
import discord4j.core.event.domain.message.ReactionAddEvent;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.Role;
import discord4j.core.object.entity.User;
import discord4j.core.object.entity.channel.Channel;
import discord4j.core.object.entity.channel.GuildMessageChannel;
import discord4j.core.object.entity.channel.MessageChannel;
import discord4j.core.object.entity.channel.TextChannel;
import discord4j.core.object.reaction.ReactionEmoji;
import discord4j.rest.util.Color;
import eu.lovitas.lsc.Vote.FinishReason;
import eu.lovitas.lsc.Vote.Type;
import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

public class LightStaticCollaborationBot {

  private static final Logger LOG = LoggerFactory.getLogger(LightStaticCollaborationBot.class);
  private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
  private static final ObjectMapper mapper = new ObjectMapper();
  private static final ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();

  private static final Pattern USER_PATTERN = Pattern.compile("name:((.(?!grund:.*))*)", Pattern.CASE_INSENSITIVE);
  private static final Pattern TAG_PATTERN = Pattern.compile("[^#]+#[0-9]{4}");
  private static final Pattern REASON_PATTERN = Pattern.compile("grund:(.+)", Pattern.CASE_INSENSITIVE);

  private static final ReactionEmoji YES_EMOJI = ReactionEmoji.unicode("\u2705");
  private static final ReactionEmoji NO_EMOJI = ReactionEmoji.unicode("\u274c");

  private static final String KICK_COMMAND = "kick";
  private static final String MUTE_COMMAND = "mute";
  private static final String STATIC_COMMAND = "static";
  private static final String VOTE_COMMAND = "vote";
  private static final String SPEEDRUN_COMMAND = "speedrun";
  private static final String LOGRUN_COMMAND = "logrun";
  private static final List<String> COMMANDS = List
      .of(KICK_COMMAND,
          MUTE_COMMAND,
          STATIC_COMMAND,
          VOTE_COMMAND,
          SPEEDRUN_COMMAND,
          LOGRUN_COMMAND);

  private static final String CONFIG_COMMAND = "config";
  private static final String VOTES_COMMAND = "votes";
  private static final List<String> ADMIN_COMMANDS = List.of(
      CONFIG_COMMAND,
      VOTES_COMMAND);

  private static Configuration configuration;
  private static Snowflake botId;
  private static GatewayDiscordClient client;

  private static RMap<Long, Vote> activeVotes;
  private static RMap<Long, Vote> finishedVotes;
  private static final Map<Long, ScheduledFuture<?>> scheduledVotes = new HashMap<>();

  public static void main(String[] args) {
    var configPathOption = Option.builder()
        .longOpt("config")
        .hasArg()
        .argName("path")
        .desc("path to the config file")
        .required()
        .build();

    var options = new Options();
    options.addOption(configPathOption);

    var formatter = new HelpFormatter();

    try {
      var commandLine = new DefaultParser().parse(options, args);
      var configPath = commandLine.getOptionValue(configPathOption.getLongOpt());

      configuration = mapper.readValue(new File(configPath), Configuration.class);

      var redisConfig = new Config();
      redisConfig.useSingleServer().setTimeout(1000000).setAddress(configuration.getRedisHost());
      var redis = Redisson.create(redisConfig);

      activeVotes = redis.getMap("votes");
      finishedVotes = redis.getMap("finishedVotes");

      var token = configuration.getToken();

      LOG.info("Loaded configuration: {}", writer.writeValueAsString(configuration));

      Optional.ofNullable(DiscordClient.create(token).login().block())
          .ifPresent(gateway -> {
            client = gateway;

            loadVotes();

            gateway.on(ReadyEvent.class)
                .subscribe(ready -> {
                  LOG.info("Logged in as {}", ready.getSelf());
                  botId = ready.getSelf().getId();
                });

            gateway.on(MemberLeaveEvent.class)
                .subscribe(LightStaticCollaborationBot::handleMemberLeaveEvent);

            gateway.on(MessageCreateEvent.class)
                .subscribe(LightStaticCollaborationBot::handleMessage);

            gateway.on(ReactionAddEvent.class)
                .subscribe(LightStaticCollaborationBot::reactionAdded);

            gateway.on(MessageDeleteEvent.class)
                .subscribe(LightStaticCollaborationBot::handleMessageDeleteEvent);

            gateway.onDisconnect().block();
          });
    } catch (ParseException e) {
      formatter.printHelp("--config=<path>", options);
      System.exit(1);
    } catch (IOException e) {
      LOG.error("Can't load configuration: ", e);
    }
  }

  private static void handleMessageDeleteEvent(MessageDeleteEvent messageDeleteEvent) {
    var channelId = messageDeleteEvent.getChannelId().asString();
    var messageId = messageDeleteEvent.getMessageId().asLong();

    if (configuration.getCommandChannelId().equals(channelId)
        || configuration.getSuggestionsChannelId().equals(channelId)
        || configuration.getLogrunVoteChannelId().equals(channelId)
        || configuration.getSpeedrunVoteChannelId().equals(channelId)) {
      var vote = activeVotes.get(messageId);

      if (vote != null) {
        activeVotes.fastRemove(messageId);
        vote.setFinishReason(FinishReason.DELETED);
        finishedVotes.fastPut(messageId, vote);
      }
    }
  }

  private static void loadVotes() {
    new ArrayList<>(activeVotes.readAllValues())
        .parallelStream()
        .forEach(vote -> {
          switch (vote.getType()) {
            case MUTE -> checkMuteVoteResult(vote);
            case SUGGESTION, SPEEDRUN, LOGRUN -> {
              var voteTime = configuration.getVoteTimes().get(vote.getType());
              var voteStartTime = vote.getStartTime();
              var votePlannedEndTime = Instant.ofEpochMilli(voteStartTime + voteTime);
              var now = Instant.now();
              if (now.isAfter(votePlannedEndTime)) {
                endVote(vote);
              } else {
                scheduledVotes.put(
                    vote.getMessageId(),
                    scheduler.schedule(
                        new ScheduledVote(vote.getMessageId()),
                        votePlannedEndTime.minusMillis(now.toEpochMilli()).toEpochMilli(),
                        TimeUnit.MILLISECONDS));
              }
            }
          }
        });
  }

  private static void handleMemberLeaveEvent(MemberLeaveEvent memberLeaveEvent) {
    var title = new StringBuilder(" hat den Server verlassen.");
    var description = new StringBuilder();
    var roleInfo = new RoleInfo();

    memberLeaveEvent.getMember()
        .ifPresentOrElse(member ->
                Optional.ofNullable(
                    member.getRoles()
                        .doOnNext(role -> roleInfo.parseRole(role, configuration))
                        .blockLast())
                    .ifPresent(__ -> {
                      title.insert(0, member.getDisplayName());
                      description.append("User hatte diese Rollen: ");
                      description.append(
                          roleInfo.getAllRoles()
                              .parallelStream()
                              .map(Role::getName)
                              .collect(Collectors.joining("\n")));
                    }),
            () -> title.insert(0, memberLeaveEvent.getUser().getTag()));

    memberLeaveEvent
        .getGuild()
        .subscribe(guild -> logAction(guild, title.toString(), description.toString()));
  }

  private static void reactionAdded(ReactionAddEvent reactionAddEvent) {
    LOG.debug("Reaction added: {}", reactionAddEvent);

    var userId = reactionAddEvent.getUserId();
    if (!userId.equals(botId)) {
      var messageId = reactionAddEvent.getMessageId();
      var vote = activeVotes.get(messageId.asLong());

      if (vote != null) {
        reactionAddEvent
            .getEmoji()
            .asUnicodeEmoji()
            .ifPresent(unicode -> {
              if (unicode.equals(YES_EMOJI)) {
                vote.addYesVote(userId.asLong());
              } else if (unicode.equals(NO_EMOJI)) {
                vote.addNoVote(userId.asLong());
              }

              activeVotes.fastPut(messageId.asLong(), vote);
              LOG.debug("Vote added: {}", vote);

              if (Boolean.TRUE.equals(configuration.getAnonymousVote().getOrDefault(vote.getType(), false))) {
                reactionAddEvent
                    .getMessage()
                    .subscribe(channel -> channel
                        .removeReaction(reactionAddEvent.getEmoji(), reactionAddEvent.getUserId())
                        .subscribe());
              }

              if (Type.MUTE.equals(vote.getType())) {
                checkMuteVoteResult(vote);
              }
            });
      }
    }
  }

  private static void checkMuteVoteResult(Vote vote) {
    client.getGuilds()
        .subscribe(guild ->
            guild.getMembers()
                .filterWhen(member -> member.getRoles()
                    .map(Role::getName)
                    .any(name -> name.equalsIgnoreCase(configuration.getStaticMentorRoleName())
                        || name.equalsIgnoreCase(configuration.getStreetworkerRoleName())))
                .reduce(new ArrayList<Member>(), (list, member) -> {
                  list.add(member);
                  return list;
                })
                .subscribe(members -> {
                  var votersOnline = members.size();

                  if (votersOnline >= configuration.getMuteVoteMinimum()) {
                    var percentage = votersOnline > configuration.getMuteVoteLowerThresholdStart()
                        ? configuration.getMuteVoteUpperThresholdPercentage()
                        : configuration.getMuteVoteLowerThresholdPercentage();

                    if (vote.getYesVotes().size() >= votersOnline / 100 * percentage) {
                      var roleInfo = new RoleInfo();
                      guild.getMemberById(Snowflake.of(vote.getTargetUserId()))
                          .subscribe(member ->
                              member.getRoles()
                                  .doOnNext(role -> roleInfo.parseRole(role, configuration))
                                  .doOnComplete(() -> {
                                    activeVotes.fastRemove(vote.getMessageId());
                                    muteUser(guild, member, roleInfo, vote.getReason());
                                  })
                                  .subscribe());
                    }
                  }
                }));
  }

  private static void handleMessage(MessageCreateEvent messageCreateEvent) {
    var message = messageCreateEvent.getMessage();
    var channelId = message.getChannelId().asString();
    var content = message.getContent();
    var commandPrefix = configuration.getCommandPrefix();

    message
        .getChannel()
        .subscribe(channel -> {
          if (Channel.Type.DM.equals(channel.getType())
              && message.getAuthor()
              .map(User::getId)
              .map(Snowflake::asLong)
              .map(id -> configuration.getAdmins().contains(id))
              .orElse(false)) {
            if (content.startsWith(commandPrefix + CONFIG_COMMAND)) {
              try {
                var configAsString = writer.writeValueAsString(configuration);
                channel.createMessage("```" + configAsString + "```").subscribe();
              } catch (JsonProcessingException e) {
                LOG.error("Error writing configuration:", e);
              }
            } else if (content.startsWith(commandPrefix + VOTES_COMMAND)) {
              handleVotesAdminCommand(channel);
            }
          } else if (content.startsWith(commandPrefix + VOTE_COMMAND + " ")) {
            var voteText = content.replaceFirst(Pattern.quote(commandPrefix + VOTE_COMMAND + " "), "").trim();

            if (configuration.getSuggestionsChannelId().equals(channelId)) {
              createVote(message, new Vote(voteText, Type.SUGGESTION));
            } else if (configuration.getSpeedrunVoteChannelId().equals(channelId)) {
              createVote(message, new Vote(voteText, Type.SPEEDRUN));
            } else if (configuration.getLogrunVoteChannelId().equals(channelId)) {
              createVote(message, new Vote(voteText, Type.LOGRUN));
            }
          } else if (configuration.getCommandChannelId().equals(channelId)
              && COMMANDS.stream().anyMatch(command -> content.startsWith(commandPrefix + command + " "))) {
            getData(message).subscribe(LightStaticCollaborationBot::handleCommand);
          }
        });
  }

  private static void handleVotesAdminCommand(MessageChannel channel) {
    client.getGuilds()
        .subscribe(guild -> {
          var activeVotesList = activeVotes.readAllValues();
          var finishedVotesList = finishedVotes.readAllValues();

          var allVotes = new ArrayList<>(activeVotesList);
          allVotes.addAll(finishedVotesList);

          allVotes
              .forEach(vote -> {
                if (activeVotes.containsKey(vote.getMessageId()) && scheduledVotes.containsKey(vote.getMessageId())) {
                  vote.setRemainingTime(scheduledVotes.get(vote.getMessageId()).getDelay(TimeUnit.MILLISECONDS));
                }

                try {
                  guild.getChannelById(getChannelIdForVote(vote))
                      .ofType(TextChannel.class)
                      .flatMap(textChannel -> textChannel.getMessageById(Snowflake.of(vote.getMessageId())))
                      .doOnSuccess(__ -> vote.setMessageExists(true))
                      .doOnError(__ -> vote.setMessageExists(false))
                      .block();
                } catch (Exception e) {
                  // debug because it is an expected exception, no need to log as error
                  LOG.debug("Error getting message for vote", e);
                }
              });

          try {
            channel
                .createMessage("***ACTIVE VOTES***\n```" +
                    writer.writeValueAsString(
                        activeVotesList.stream()
                            .sorted((v1, v2) -> v1.getRemainingTime() <= v2.getRemainingTime() ? -1 : 1)
                            .collect(Collectors.toList())) +
                    "```")
                .subscribe();
            channel
                .createMessage("***FINISHED VOTES***\n```" + writer.writeValueAsString(finishedVotesList) + "```")
                .subscribe();
          } catch (JsonProcessingException e) {
            LOG.error("Error writing votes as string: ", e);
          }
        });
  }

  private static Snowflake getChannelIdForVote(Vote vote) {
    return switch (vote.getType()) {
      case MUTE -> Snowflake.of(configuration.getCommandChannelId());
      case SUGGESTION -> Snowflake.of(configuration.getSuggestionsChannelId());
      case SPEEDRUN -> Snowflake.of(configuration.getSpeedrunVoteChannelId());
      case LOGRUN -> Snowflake.of(configuration.getLogrunVoteChannelId());
    };
  }

  private static Mono<CommandData> getData(Message message) {
    var content = message.getContent();

    LOG.info("Command received: {}", content);

    var commandEnd = content.indexOf(' ');
    var command = content.substring(1, commandEnd > -1 ? commandEnd : content.length());

    var commandData = new CommandData(command, message);
    var authorRoleInfo = commandData.getAuthorRoleInfo();

    return message
        .getGuild()
        .doOnError(err -> LOG.error("Couldn't get guild of command message: {}", message))
        .flatMap(guild -> {
          commandData.setGuild(guild);
          return message
              .getAuthorAsMember()
              .doOnError(err -> LOG.error("Couldn't get author of command message: {}", message))
              .flatMap(member -> {
                commandData.setAuthor(member);
                return member
                    .getRoles()
                    .doOnError(err -> LOG.error("Couldn't get roles for member: {}", member))
                    .doOnNext(role -> authorRoleInfo.parseRole(role, configuration))
                    .last()
                    .map(role -> commandData);
              });
        });
  }

  private static void handleCommand(CommandData commandData) {
    var message = commandData.getMessage();

    if (canExecute(commandData)) {
      getUser(commandData).subscribe(
          potentialUsers -> {
            var userName = potentialUsers.getKey();
            var potentialUserList = potentialUsers.getValue();

            if (potentialUserList.isEmpty()) {
              noUserFound(message, userName);
            } else if (potentialUserList.size() > 1) {
              tooManyPotentialUsers(
                  message,
                  userName,
                  potentialUserList
                      .stream()
                      .map(Member::getDisplayName)
                      .collect(Collectors.toList()));
            } else {
              handleCommandWithUser(commandData, potentialUserList.get(0));
            }
          });
    } else {
      noPermission(commandData);
    }
  }

  private static boolean canExecute(CommandData commandData) {
    var command = commandData.getCommand().toLowerCase();
    var authorRoleInfo = commandData.getAuthorRoleInfo();

    return switch (command) {
      case KICK_COMMAND, MUTE_COMMAND -> authorRoleInfo.isStaticMentor() || authorRoleInfo.isStreetWorker();
      case STATIC_COMMAND -> authorRoleInfo.isStaticMentor();
      case SPEEDRUN_COMMAND -> authorRoleInfo.isSpeedRunModerator();
      case LOGRUN_COMMAND -> authorRoleInfo.isLogRunModerator();
      default -> false;
    };
  }

  private static boolean canExecuteForUser(CommandData commandData) {
    var command = commandData.getCommand().toLowerCase();
    var authorRoleInfo = commandData.getAuthorRoleInfo();
    var userRoleInfo = commandData.getUserRoleInfo();

    return switch (command) {
      case KICK_COMMAND -> hasRightsForUser(commandData);
      case STATIC_COMMAND -> authorRoleInfo.isStaticMentor()
          && authorRoleInfo.getStaticRole() != null
          && (userRoleInfo.getStaticRole() == null
          || authorRoleInfo.getStaticRole().equals(userRoleInfo.getStaticRole()));
      case MUTE_COMMAND -> authorRoleInfo.isStaticMentor() || authorRoleInfo.isStreetWorker();
      case SPEEDRUN_COMMAND -> authorRoleInfo.isSpeedRunModerator();
      case LOGRUN_COMMAND -> authorRoleInfo.isLogRunModerator();
      default -> false;
    };
  }

  private static boolean hasRightsForUser(CommandData commandData) {
    var authorRoleInfo = commandData.getAuthorRoleInfo();
    var userRoleInfo = commandData.getUserRoleInfo();

    return (authorRoleInfo.isStaticMentor() && authorRoleInfo.getStaticRole().equals(userRoleInfo.getStaticRole()))
        || (authorRoleInfo.isStreetWorker() && (userRoleInfo.isHomeless() || userRoleInfo.isSolo()));
  }

  private static void handleCommandWithUser(CommandData commandData, Member user) {
    var command = commandData.getCommand();

    switch (command) {
      case KICK_COMMAND -> getReason(commandData)
          .ifPresent(reason -> getUserRolesAndExecute(commandData, user, () -> kickUser(commandData, user, reason)));
      case MUTE_COMMAND -> getReason(commandData)
          .ifPresent(reason -> getUserRolesAndExecute(commandData, user, () -> muteUser(commandData, user, reason)));
      case SPEEDRUN_COMMAND, LOGRUN_COMMAND, STATIC_COMMAND -> getUserRolesAndExecute(commandData, user,
          () -> assignRole(commandData, user));
    }
  }

  private static void getUserRolesAndExecute(CommandData commandData, Member user, Runnable onComplete) {
    var userRoleInfo = commandData.getUserRoleInfo();
    user.getRoles()
        .doOnError(err -> LOG.error("Couldn't get roles for member: {}", user))
        .doOnComplete(() -> {
          if (canExecuteForUser(commandData)) {
            onComplete.run();
          } else {
            noPermission(commandData);
          }
        })
        .subscribe(role -> userRoleInfo.parseRole(role, configuration));
  }

  private static void kickUser(CommandData commandData, Member user, String reason) {
    var guild = commandData.getGuild();
    guild.kick(user.getId(), reason)
        .doOnSuccess(
            ignored -> logAction(guild,
                user.getDisplayName() + " wurde gekicked!",
                "Grund: " + reason
                    + "\nUser hatte diese extra Rollen:\n"
                    + String.join("\n", commandData.getUserRoleInfo().getSpecialRoles())))
        .subscribe();
  }

  private static void muteUser(CommandData commandData, Member user, String reason) {
    if (hasRightsForUser(commandData)) {
      muteUser(commandData.getGuild(), user, commandData.getUserRoleInfo(), reason);
    } else {
      createMuteVote(commandData, user, reason);
    }
  }

  private static void muteUser(Guild guild, Member user, RoleInfo userRoleInfo, String reason) {
    user.getRoles()
        .doOnComplete(() ->
            guild.getRoles()
                .filter(role -> role.getName().equalsIgnoreCase(configuration.getMutedRoleName()))
                .subscribe(role ->
                    user.addRole(role.getId(), reason)
                        .doOnSuccess(
                            ignored -> logAction(guild,
                                user.getDisplayName() + " hat einen Maulkorb bekommen!",
                                "Grund: " + reason
                                    + "\nUser hatte diese extra Rollen:\n"
                                    + String.join("\n", userRoleInfo.getSpecialRoles())))
                        .subscribe()))
        .subscribe(role -> {
          if (configuration.getSpecialRoles().parallelStream().anyMatch(r -> role.getName().equalsIgnoreCase(r))) {
            user.removeRole(role.getId()).subscribe();
          }
        });
  }

  private static void createMuteVote(CommandData commandData, Member user, String reason) {
    createVote(commandData.getMessage(),
        new Vote(user.getDisplayName() + " einen Maulkorb geben?", Type.MUTE, user.getId().asLong(), reason));
  }

  private static void createVote(Message message, Vote vote) {
    message
        .getChannel()
        .subscribe(channel ->
            channel.createEmbed(spec ->
                spec.setColor(Color.BLUE)
                    .setTitle(vote.getVoteText()))
                .subscribe(embed -> {
                  var messageId = embed.getId().asLong();

                  embed
                      .addReaction(YES_EMOJI)
                      .subscribe();
                  embed
                      .addReaction(NO_EMOJI)
                      .subscribe();

                  activeVotes.fastPut(messageId, vote.setMessageId(messageId));

                  if (!Type.MUTE.equals(vote.getType())) {
                    scheduledVotes.put(
                        vote.getMessageId(),
                        scheduler.schedule(
                            new ScheduledVote(vote.getMessageId()),
                            configuration.getVoteTimes().get(vote.getType()),
                            TimeUnit.MILLISECONDS));
                  }

                  try {
                    message.delete().subscribe();
                  } catch (Exception e) {
                    LOG.error("Can't delete original vote message: ", e);
                  }
                }));
  }

  public static void endVote(long messageId) {
    if (!activeVotes.containsKey(messageId)) {
      return;
    }

    endVote(activeVotes.get(messageId));
  }

  public static void endVote(Vote vote) {
    if (!activeVotes.containsKey(vote.getMessageId())) {
      return;
    }

    LOG.info("Vote ended: {}", vote);

    activeVotes.fastRemove(vote.getMessageId());
    vote.setFinishReason(FinishReason.TIME);
    finishedVotes.fastPut(vote.getMessageId(), vote);

    client.getGuilds()
        .subscribe(guild ->
            guild.getChannelById(getChannelIdForVote(vote))
                .ofType(TextChannel.class)
                .doOnSuccess(textChannel ->
                    textChannel.getMessageById(Snowflake.of(vote.getMessageId()))
                        .doOnSuccess(message -> {
                          message.delete().subscribe();
                          textChannel.createEmbed(spec ->
                              spec.setTitle("Abstimmung beendet!")
                                  .setDescription(vote.toResultText())
                                  .setColor(Color.BLUE))
                              .subscribe();
                        })
                        .doOnError(__ -> LOG.error("Couldn't find message for vote {}", vote))
                        .subscribe())
                .doOnError(__ -> LOG.error("Couldn't find channel for vote {}", vote))
                .subscribe());
  }

  private static void assignRole(CommandData commandData, Member user) {
    var command = commandData.getCommand();
    var authorStaticRole = commandData.getAuthorRoleInfo().getStaticRole().getName();
    var speedrunRoleName = configuration.getSpeedrunRoleName();
    var logrunRoleName = configuration.getLogrunRoleName();

    switch (command) {
      case STATIC_COMMAND -> assignRole(
          commandData,
          user,
          authorStaticRole,
          user.getDisplayName() + " wurde der Static '" + authorStaticRole + "' zugewiesen!",
          user.getDisplayName() + " wurde aus der Static '" + authorStaticRole + "' entfernt!");
      case SPEEDRUN_COMMAND -> assignRole(
          commandData,
          user,
          speedrunRoleName,
          user.getDisplayName() + " wurde die Rolle '" + speedrunRoleName + "' zugewiesen!",
          user.getDisplayName() + " wurde die Rolle '" + speedrunRoleName + "' weggenommen!");
      case LOGRUN_COMMAND -> assignRole(
          commandData,
          user,
          logrunRoleName,
          user.getDisplayName() + " wurde die Rolle '" + logrunRoleName + "' zugewiesen!",
          user.getDisplayName() + " wurde die Rolle '" + logrunRoleName + "' weggenommen!");
    }
  }

  private static void assignRole(CommandData commandData, Member user, String roleName, String addTitle,
      String removeTitle) {
    var guild = commandData.getGuild();

    guild.getRoles().filter(role -> role.getName().equalsIgnoreCase(roleName))
        .subscribe(role -> {
          if (commandData.getUserRoleInfo().getAllRoles().contains(role)) {
            user.removeRole(role.getId())
                .doOnSuccess(ignored -> logAction(guild, removeTitle, ""))
                .subscribe();
          } else {
            user.addRole(role.getId())
                .doOnSuccess(ignored -> logAction(guild, addTitle, ""))
                .subscribe();
          }
        });
  }

  private static Optional<String> getReason(CommandData commandData) {
    var matcher = REASON_PATTERN.matcher(commandData.getMessage().getContent());
    if (matcher.find()) {
      return Optional.of(matcher.group(1).trim());
    }

    notEnoughParameters(commandData, "grund");
    return Optional.empty();
  }

  private static Mono<SimpleEntry<String, List<Member>>> getUser(CommandData commandData) {
    var message = commandData.getMessage();
    var content = commandData.getCommandContent();

    if (content.length() == 0) {
      return Mono.empty();
    }

    var matcher = USER_PATTERN.matcher(content);
    if (matcher.find()) {
      var user = matcher.group(1).trim();
      var fullName = user.indexOf(' ') > -1;
      var tag = !fullName && TAG_PATTERN.matcher(user).matches();

      return message
          .getGuild()
          .flatMapMany(Guild::getMembers)
          .reduce(new SimpleEntry<>(user, new ArrayList<>()), (entry, member) -> {
            if ((tag && member.getTag().equalsIgnoreCase(user))
                || (fullName && member.getDisplayName().equalsIgnoreCase(user))
                || member.getDisplayName().startsWith(user)) {
              entry.getValue().add(member);
            }

            return entry;
          });
    } else {
      notEnoughParameters(commandData, "name");
      return Mono.empty();
    }
  }

  private static void notEnoughParameters(CommandData commandData, String missingParameter) {
    var message = commandData.getMessage();

    LOG.info("Not enough parameters provided in: {}", commandData.getMessage().getContent());

    message.getChannel()
        .subscribe(channel ->
            channel.createEmbed(spec ->
                spec.setColor(Color.RED)
                    .setTitle("Es fehlt der Parameter: " + missingParameter))
                .subscribe());
  }

  private static void tooManyPotentialUsers(Message message, String user, List<String> potentialUsers) {
    LOG.info("More than one user found for {}: {}", user, potentialUsers);

    message.getChannel().subscribe(channel ->
        channel.createEmbed(spec ->
            spec.setColor(Color.RED)
                .setTitle("Mehrere User für '" + user + "' gefunden!")
                .setDescription(String.join("\n", potentialUsers)))
            .subscribe());
  }

  private static void noPermission(CommandData commandData) {
    var message = commandData.getMessage();
    LOG.info("User {} has no permission to execute: {}", commandData.getAuthor(), message.getContent());

    message.getChannel().subscribe(channel ->
        channel.createEmbed(spec ->
            spec.setColor(Color.RED)
                .setTitle("Das darfst Du nicht!")
                .setDescription("Diesen Befehl darfst du so nicht ausführen!"))
            .subscribe());
  }

  private static void noUserFound(Message message, String user) {
    LOG.info("No user found for {}", user);

    message.getChannel().subscribe(channel ->
        channel.createEmbed(spec ->
            spec.setColor(Color.RED)
                .setTitle("Keinen User mit dem Namen: '" + user + "' gefunden!"))
            .subscribe());
  }

  private static void logAction(Guild guild, String title, String description) {
    LOG.info("Action executed: {}: {}", title, description);

    guild.getChannelById(Snowflake.of(configuration.getLogChannelId()))
        .ofType(GuildMessageChannel.class)
        .subscribe(channel ->
            channel.createEmbed(spec ->
                spec.setColor(Color.WHITE)
                    .setTitle(title)
                    .setDescription(description)
                    .setTimestamp(Instant.now()))
                .subscribe());
  }
}
