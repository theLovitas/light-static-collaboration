package eu.lovitas.lsc;

import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Message;
import java.util.regex.Pattern;

public class CommandData {

  private final String command;
  private final Message message;
  private final RoleInfo authorRoleInfo = new RoleInfo();
  private final RoleInfo userRoleInfo = new RoleInfo();

  private Guild guild;
  private Member author;

  public CommandData(String command, Message message) {
    this.command = command;
    this.message = message;
  }

  public String getCommand() {
    return command;
  }

  public Message getMessage() {
    return message;
  }

  public Guild getGuild() {
    return guild;
  }

  public void setGuild(Guild guild) {
    this.guild = guild;
  }

  public Member getAuthor() {
    return author;
  }

  public void setAuthor(Member author) {
    this.author = author;
  }

  public RoleInfo getAuthorRoleInfo() {
    return authorRoleInfo;
  }

  public RoleInfo getUserRoleInfo() {
    return userRoleInfo;
  }

  public String getCommandContent() {
    return message.getContent().substring(1).replaceFirst(Pattern.quote(command), "").trim();
  }
}
