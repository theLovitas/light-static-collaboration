package eu.lovitas.lsc;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

public class Vote implements Serializable {

  private long messageId;
  private long startTime;
  private final String voteText;
  private final Type type;
  private long targetUserId;
  private String reason;

  // debug information
  private boolean messageExists = false;
  private Long remainingTime;
  private FinishReason finishReason;

  private final Set<Long> yesVotes = new HashSet<>();
  private final Set<Long> noVotes = new HashSet<>();

  public Vote(String voteText, Type type) {
    this.voteText = voteText;
    this.type = type;
  }

  public Vote(String voteText, Type type, long targetUserId, String reason) {
    this(voteText, type);
    this.targetUserId = targetUserId;
    this.reason = reason;
  }

  public long getMessageId() {
    return messageId;
  }

  public Vote setMessageId(long messageId) {
    this.messageId = messageId;
    this.startTime = Instant.now().toEpochMilli();
    return this;
  }

  public long getStartTime() {
    return startTime;
  }

  public String getVoteText() {
    return voteText;
  }

  public Type getType() {
    return type;
  }

  public long getTargetUserId() {
    return targetUserId;
  }

  public String getReason() {
    return reason;
  }

  public Set<Long> getYesVotes() {
    return yesVotes;
  }

  public Set<Long> getNoVotes() {
    return noVotes;
  }

  public void addYesVote(long userId) {
    yesVotes.add(userId);
    noVotes.remove(userId);
  }

  public void addNoVote(long userId) {
    noVotes.add(userId);
    yesVotes.remove(userId);
  }

  public boolean isMessageExists() {
    return messageExists;
  }

  public void setMessageExists(boolean messageExists) {
    this.messageExists = messageExists;
  }

  public Long getRemainingTime() {
    return remainingTime;
  }

  public void setRemainingTime(Long remainingTime) {
    this.remainingTime = remainingTime;
  }

  public FinishReason getFinishReason() {
    return finishReason;
  }

  public void setFinishReason(FinishReason finishReason) {
    this.finishReason = finishReason;
  }

  public String toResultText() {
    return String.format("%s: JA:%d NEIN:%d", voteText, yesVotes.size(), noVotes.size());
  }

  enum Type {
    MUTE, SUGGESTION, SPEEDRUN, LOGRUN
  }

  enum FinishReason {
    TIME, DELETED
  }
}
