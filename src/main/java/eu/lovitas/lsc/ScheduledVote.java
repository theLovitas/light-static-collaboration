package eu.lovitas.lsc;

public class ScheduledVote implements Runnable {

  private final long messageId;

  public ScheduledVote(long messageId) {
    this.messageId = messageId;
  }

  @Override
  public void run() {
    LightStaticCollaborationBot.endVote(messageId);
  }
}
