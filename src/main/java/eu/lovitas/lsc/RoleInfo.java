package eu.lovitas.lsc;

import discord4j.core.object.entity.Role;
import java.util.ArrayList;
import java.util.List;

public class RoleInfo {

  private final List<String> specialRoles = new ArrayList<>();
  private final List<Role> allRoles = new ArrayList<>();

  private Role staticRole;
  private boolean moderator = false;
  private boolean speedRunModerator = false;
  private boolean logRunModerator = false;
  private boolean staticMentor = false;
  private boolean homeless = false;
  private boolean streetWorker = false;
  private boolean solo = false;

  public List<String> getSpecialRoles() {
    return specialRoles;
  }

  public List<Role> getAllRoles() {
    return allRoles;
  }

  public Role getStaticRole() {
    return staticRole;
  }

  public void setStaticRole(Role staticRole) {
    this.staticRole = staticRole;
  }

  public boolean isModerator() {
    return moderator;
  }

  public void setModerator(boolean moderator) {
    this.moderator = moderator;
  }

  public boolean isSpeedRunModerator() {
    return speedRunModerator;
  }

  public void setSpeedRunModerator(boolean speedRunModerator) {
    this.speedRunModerator = speedRunModerator;
  }

  public boolean isLogRunModerator() {
    return logRunModerator;
  }

  public void setLogRunModerator(boolean logRunModerator) {
    this.logRunModerator = logRunModerator;
  }

  public boolean isStaticMentor() {
    return staticMentor;
  }

  public void setStaticMentor(boolean staticMentor) {
    this.staticMentor = staticMentor;
  }

  public boolean isHomeless() {
    return homeless;
  }

  public void setHomeless(boolean homeless) {
    this.homeless = homeless;
  }

  public boolean isStreetWorker() {
    return streetWorker;
  }

  public void setStreetWorker(boolean streetWorker) {
    this.streetWorker = streetWorker;
  }

  public boolean isSolo() {
    return solo;
  }

  public void setSolo(boolean solo) {
    this.solo = solo;
  }

  public void parseRole(Role role, Configuration configuration) {
    allRoles.add(role);
    var identified = false;
    var name = role.getName();

    if (name.equalsIgnoreCase(configuration.getModeratorRoleName())) {
      setModerator(true);
      identified = true;
    }

    if (name.equalsIgnoreCase(configuration.getModeratorRoleName())) {
      setModerator(true);
      identified = true;
    }

    if (name.equalsIgnoreCase(configuration.getSpeedrunModeratorRoleName())) {
      setSpeedRunModerator(true);
      identified = true;
    }

    if (name.equalsIgnoreCase(configuration.getLogrunModeratorRoleName())) {
      setLogRunModerator(true);
      identified = true;
    }

    if (name.equalsIgnoreCase(configuration.getStaticMentorRoleName())) {
      setStaticMentor(true);
      identified = true;
    }

    if (name.equalsIgnoreCase(configuration.getHomelessRoleName())) {
      setHomeless(true);
      identified = true;
    }

    if (name.equalsIgnoreCase(configuration.getStreetworkerRoleName())) {
      setStreetWorker(true);
      identified = true;
    }

    if (name.equalsIgnoreCase(configuration.getSoloRoleName())) {
      setSolo(true);
      identified = true;
    }

    if (!identified
        && !name.equalsIgnoreCase(configuration.getStaticRoleName())
        && !name.equalsIgnoreCase(configuration.getMutedRoleName())) {
      if (configuration.getSpecialRoles()
          .parallelStream()
          .noneMatch(name::equalsIgnoreCase)) {
        setStaticRole(role);
      } else {
        getSpecialRoles().add(role.getName());
      }
    }
  }
}
