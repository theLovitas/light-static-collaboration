package eu.lovitas.lsc;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.lovitas.lsc.Vote.Type;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Configuration {

  private String token;
  private String logChannelId;
  private String commandChannelId;
  private String suggestionsChannelId;
  private String speedrunVoteChannelId;
  private String logrunVoteChannelId;

  private String commandPrefix = ".";

  private String moderatorRoleName = "Moderator";
  private String speedrunModeratorRoleName = "Speed & Logrun Moderator";
  private String logrunModeratorRoleName = "Speed & Logrun Moderator";
  private String staticMentorRoleName = "Static Mentor";
  private String staticRoleName = "Static";
  private String homelessRoleName = "Obdachlos";
  private String mutedRoleName = "Maulkorb";
  private String streetworkerRoleName = "Streetworker";
  private String soloRoleName = "Solo";
  private String speedrunRoleName = "Speedruns";
  private String logrunRoleName = "Logruns";
  private List<String> specialRoles = List
      .of("Die Legende",
          "Die ultimative Legende",
          "Die perfekte Legende",
          "Bot Support",
          speedrunRoleName,
          logrunRoleName,
          speedrunModeratorRoleName,
          logrunModeratorRoleName);

  private Map<Type, Boolean> anonymousVote = new EnumMap<>(Map.of(
      Type.SUGGESTION, false,
      Type.SPEEDRUN, true,
      Type.LOGRUN, true
  ));
  private Map<Type, Long> voteTimes = new EnumMap<>(Map.of(
      Type.SUGGESTION, 172800000L,
      Type.SPEEDRUN, 86400000L,
      Type.LOGRUN, 86400000L
  ));

  private int muteVoteMinimum = 3;
  private int muteVoteLowerThresholdStart = 4;
  private int muteVoteLowerThresholdPercentage = 50;
  private int muteVoteUpperThresholdPercentage = 60;

  private List<Long> admins = List.of(98139029433745408L, 334099752666791937L);

  private String redisHost = "redis://redis:6379";

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getLogChannelId() {
    return logChannelId;
  }

  public void setLogChannelId(String logChannelId) {
    this.logChannelId = logChannelId;
  }

  public String getCommandChannelId() {
    return commandChannelId;
  }

  public void setCommandChannelId(String commandChannelId) {
    this.commandChannelId = commandChannelId;
  }

  public String getSuggestionsChannelId() {
    return suggestionsChannelId;
  }

  public void setSuggestionsChannelId(String suggestionsChannelId) {
    this.suggestionsChannelId = suggestionsChannelId;
  }

  public String getSpeedrunVoteChannelId() {
    return speedrunVoteChannelId;
  }

  public void setSpeedrunVoteChannelId(String speedrunVoteChannelId) {
    this.speedrunVoteChannelId = speedrunVoteChannelId;
  }

  public String getLogrunVoteChannelId() {
    return logrunVoteChannelId;
  }

  public void setLogrunVoteChannelId(String logrunVoteChannelId) {
    this.logrunVoteChannelId = logrunVoteChannelId;
  }

  public String getCommandPrefix() {
    return commandPrefix;
  }

  public void setCommandPrefix(String commandPrefix) {
    this.commandPrefix = commandPrefix;
  }

  public String getModeratorRoleName() {
    return moderatorRoleName;
  }

  public void setModeratorRoleName(String moderatorRoleName) {
    this.moderatorRoleName = moderatorRoleName;
  }

  public String getSpeedrunModeratorRoleName() {
    return speedrunModeratorRoleName;
  }

  public void setSpeedrunModeratorRoleName(String speedrunModeratorRoleName) {
    this.speedrunModeratorRoleName = speedrunModeratorRoleName;
  }

  public String getLogrunModeratorRoleName() {
    return logrunModeratorRoleName;
  }

  public void setLogrunModeratorRoleName(String logrunModeratorRoleName) {
    this.logrunModeratorRoleName = logrunModeratorRoleName;
  }

  public String getStaticMentorRoleName() {
    return staticMentorRoleName;
  }

  public void setStaticMentorRoleName(String staticMentorRoleName) {
    this.staticMentorRoleName = staticMentorRoleName;
  }

  public String getStaticRoleName() {
    return staticRoleName;
  }

  public void setStaticRoleName(String staticRoleName) {
    this.staticRoleName = staticRoleName;
  }

  public String getHomelessRoleName() {
    return homelessRoleName;
  }

  public void setHomelessRoleName(String homelessRoleName) {
    this.homelessRoleName = homelessRoleName;
  }

  public String getMutedRoleName() {
    return mutedRoleName;
  }

  public void setMutedRoleName(String mutedRoleName) {
    this.mutedRoleName = mutedRoleName;
  }

  public String getStreetworkerRoleName() {
    return streetworkerRoleName;
  }

  public void setStreetworkerRoleName(String streetworkerRoleName) {
    this.streetworkerRoleName = streetworkerRoleName;
  }

  public String getSoloRoleName() {
    return soloRoleName;
  }

  public void setSoloRoleName(String soloRoleName) {
    this.soloRoleName = soloRoleName;
  }

  public String getSpeedrunRoleName() {
    return speedrunRoleName;
  }

  public void setSpeedrunRoleName(String speedrunRoleName) {
    this.speedrunRoleName = speedrunRoleName;
  }

  public String getLogrunRoleName() {
    return logrunRoleName;
  }

  public void setLogrunRoleName(String logrunRoleName) {
    this.logrunRoleName = logrunRoleName;
  }

  public List<String> getSpecialRoles() {
    return specialRoles;
  }

  public void setSpecialRoles(List<String> specialRoles) {
    this.specialRoles = specialRoles;
  }

  public Map<Type, Boolean> getAnonymousVote() {
    return anonymousVote;
  }

  public void setAnonymousVote(Map<Type, Boolean> anonymousVote) {
    this.anonymousVote = anonymousVote;
  }

  public Map<Type, Long> getVoteTimes() {
    return voteTimes;
  }

  public void setVoteTimes(Map<Type, Long> voteTimes) {
    this.voteTimes = voteTimes;
  }

  public int getMuteVoteMinimum() {
    return muteVoteMinimum;
  }

  public void setMuteVoteMinimum(int muteVoteMinimum) {
    this.muteVoteMinimum = muteVoteMinimum;
  }

  public int getMuteVoteLowerThresholdStart() {
    return muteVoteLowerThresholdStart;
  }

  public void setMuteVoteLowerThresholdStart(int muteVoteLowerThresholdStart) {
    this.muteVoteLowerThresholdStart = muteVoteLowerThresholdStart;
  }

  public int getMuteVoteLowerThresholdPercentage() {
    return muteVoteLowerThresholdPercentage;
  }

  public void setMuteVoteLowerThresholdPercentage(int muteVoteLowerThresholdPercentage) {
    this.muteVoteLowerThresholdPercentage = muteVoteLowerThresholdPercentage;
  }

  public int getMuteVoteUpperThresholdPercentage() {
    return muteVoteUpperThresholdPercentage;
  }

  public void setMuteVoteUpperThresholdPercentage(int muteVoteUpperThresholdPercentage) {
    this.muteVoteUpperThresholdPercentage = muteVoteUpperThresholdPercentage;
  }

  public List<Long> getAdmins() {
    return admins;
  }

  public void setAdmins(List<Long> admins) {
    this.admins = admins;
  }

  public String getRedisHost() {
    return redisHost;
  }

  public void setRedisHost(String redisHost) {
    this.redisHost = redisHost;
  }
}
