#
# Build stage
#
FROM maven:3.6.3-adoptopenjdk-14 AS build
COPY src /home/lsc/src
COPY pom.xml /home/lsc
RUN mvn -f /home/lsc/pom.xml clean package

#
# Package stage
#
FROM adoptopenjdk:14.0.2_12-jre-hotspot-focal
COPY --from=build /home/lsc/target/light-static-collaboration-bot-shaded.jar /usr/local/lib/lsc.jar
ENTRYPOINT ["java","-jar","/usr/local/lib/lsc.jar","--config=/home/lsc/config/configuration.json"]
